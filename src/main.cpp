/*
NTP synchronized clock with watering system
author Dorian Obermaier
description: tbd
*/

#include <Arduino.h>
#include <WiFi.h>
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
#include <driver/gpio.h>
#include <driver/timer.h>
#include <driver/adc.h>

#define I2C_SDA 21
#define I2C_SCL 22

#define CRITICAL_YEAR 2036
#define SECONDS_PER_YEAR 60*60*24*365 //seconds
#define SECONDS_PER_DAY 86400 //seconds
#define LEAP_INTERVAL ((4*SECONDS_PER_YEAR)+SECONDS_PER_DAY)  //seconds
#define MONTH_PER_YEAR 12
#define HOURS_PER_DAY 24
#define MINUTES_PER_HOUR 60
#define SECONDS_PER_MINUTE 60
#define T_INTR_INTERVAL 1000000 //µs
#define SECOND 1000 //ms

#define MOISTURE_0 ADC1_CHANNEL_4 //GPIO32
#define MOISTURE_1 ADC1_CHANNEL_5 //GPIO33
#define PUMP GPIO_NUM_25
#define SOLENOID_0 GPIO_NUM_26
#define SOLENOID_1 GPIO_NUM_27
#define PIR_PIN GPIO_NUM_18
#define DRY 3000 //adc range ~1200-3500 from capacitive moisture sensor
#define MEAS_SAMPLE_PAUSE 100 //ms
#define WATERING_INTERVAL (5*SECOND)
#define ON 1
#define OFF 0
#define CYCLES 3  //for safety: number of watering cycles before watering routine is suspended and alarm is set

hw_timer_t *timer1 = NULL;
hw_timer_t *timer2 = NULL;

TaskHandle_t task_time;
TaskHandle_t task_watering;
WiFiUDP udp;
LiquidCrystal_PCF8574 lcd(0x3F);  //0x3F //0x27
//WiFi
void connect_WiFi(char [],char []);
void disconnect_WiFi(void);
void send_UDP_packet(uint8_t[],int,IPAddress,int);
//NTP
void recieve_UDP_packet(uint8_t[]);
uint32_t extract_time(uint8_t[]);
//time
void calc_date(uint32_t);
void calc_season_shift(uint16_t,uint16_t);
void rollover(void);
//peripheral configuration
void measure(void);
void switch_actuator(gpio_num_t,bool);
void watering_routine(void);
void update_output(void);
void hardware_config(void);
//some functions
void wait_animation(void);
void custom_char_animtion(void);
int* get_custom(void);
//tasks
void refresh_time( void * pvParameters );
void check_plants( void * pvParameters );
//ISR handler
void IRAM_ATTR count_ISR(void);
void IRAM_ATTR pir_ISR(void);
//time variables
enum Month:uint8_t {jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec} ;
enum Day:uint8_t {mon, tue, wed, thu, fri, sat, sun};
String day_verbose [] = {"MON","TUE","WED","THU","FRI","SAT","SUN"};
struct timedate{
  uint8_t second;
  uint8_t minute;
  uint8_t hour;

  uint8_t dd;
  uint8_t mm;
  uint16_t yy;

  bool leap_indicator = false;
  uint8_t passed_days; //since 1900
  uint8_t day_label = 0;
  uint8_t days_in_month[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
  uint8_t season_shift[2] = {0,0};  //{day of march, day of october}first value is the day of switching to summer time (+1hour),
                                    //second value is the day of switching to winter time (-1hour)
  bool shifted = false; //flag for season shift. relevant for witching to winter time as the same time will be passed twice (03:00:00)
}current_time;
//control variables
bool t_update;
bool d_update;
bool motion;
bool isr_flag;
bool display_on;
bool take_sample;
bool watering_activated;
bool dry_flag[2] = {false,false};
int cycle_counter[2] = {0,0};
bool pump_alarm = false;
bool routine_done[2] = {false, false};
//other variables
//some custom characters for lcd fun
int twoleg[8] = {	0b01110,	0b11111,	0b10101,	0b11111,	0b11111,	0b01110,	0b01010,	0b11011};
int hourglass[8] = {	0b00000,	0b11111,	0b11111,	0b01110,	0b00100,	0b01110,	0b11111,	0b11111};
int lock[8] = {	0b00000,	0b01110,	0b10001,	0b10001,	0b11111,	0b11011,	0b11011,	0b11111};
int unicorn[8] = {	0b00100,	0b00100,	0b10101,	0b11111,	0b10101,	0b11111,	0b01110,	0b01110};
int hippo[8] = {	0b10001,	0b01110,	0b10101,	0b11111,	0b01110,	0b11111,	0b11111,	0b01110};
int skull[8] = {	0b10001,	0b01110,	0b11111,	0b11111,	0b10101,	0b01110,	0b01110,	0b10001};
int spider[8] = {	0b01100,	0b10110,	0b11101,	0b10111,	0b01110,	0b10101,	0b10101,	0b10101};
int umbrella[8] = {	0b00100,	0b01110,	0b11111,	0b11111,	0b00100,	0b00100,	0b00101,	0b00111};
int deer[8] = {	0b01010,	0b11011,	0b01010,	0b11111,	0b10101,	0b11111,	0b01110,	0b00100};
int mouse[8] = {	0b00000,	0b01010,	0b01110,	0b11111,	0b10101,	0b11111,	0b01110,	0b00100};
int bandit[8] = {	0b00000,	0b00000,	0b11111,	0b10101,	0b11111,	0b00000,	0b01110,	0b10001};
int happy[8] = {	0b11111,	0b11111,	0b00000,	0b01010,	0b00000,	0b11111,	0b10001,	0b01110};
int trophy[8] = {	0b00000,	0b11111,	0b01110,	0b01110,	0b01110,	0b00100,	0b00100,	0b01110};
int human[8] = {	0b01100,	0b01100,	0b00100,	0b01110,	0b10101,	0b00100,	0b01010,	0b01010};
int giraffe[8] = {	0b01000,	0b11000,	0b01000,	0b01000,	0b01000,	0b01111,	0b01010,	0b01010};
int eye[8] = {  0b00100,  0b01110,   0b10001,  0b10101,  0b10101,  0b10001,  0b01110, 0b00100};
int custom_c = 0;

//_________________________________________________________________________setup
//
void setup()
{
  char ssid [] = "SSID";
  char passwd [] = "PW";
  int udp_port = 123;
  int ntp_port = 123;
  IPAddress address_Uni_KL (131,246,9,116);
  IPAddress address_Uni_FFM (141,2,22,74);
  uint8_t buffer [48] = {0};

  Wire.begin(I2C_SDA,I2C_SCL);
  lcd.begin(2,16);
  lcd.setBacklight(255);
  custom_char_animtion();

  Serial.begin(115200);
  while(!Serial){}

  connect_WiFi(ssid,passwd);
  send_UDP_packet(buffer,udp_port,address_Uni_KL,ntp_port);
  recieve_UDP_packet(buffer);
  disconnect_WiFi();
  calc_date(extract_time(buffer));
  measure();
  delay(5000);
  xTaskCreatePinnedToCore(refresh_time,"Refresh_Time_Task",20000,NULL,1,&task_time,1);
  xTaskCreatePinnedToCore(check_plants,"Check_Plants_Task",20000,NULL,1,&task_watering,1);
  lcd.clear();
  lcd.noDisplay();
  lcd.setBacklight(0);

  hardware_config();

  t_update = false;
  d_update = true;
  motion = false;
  isr_flag = false;
  display_on = false;
  take_sample = false;
  watering_activated = false;
}
//_________________________________________________________________________time task

void refresh_time( void * pvParameters )
{
    printf("refresh_time() running on core: %i\n",xPortGetCoreID());
    for( ;; )
    {
      if(current_time.second == SECONDS_PER_MINUTE)
      {
        rollover();
      }
      if(motion && t_update) //if motion was detected and time has changed, output update is necessary
      {
        display_on = true;
        update_output();
        //printf("display: on\n");
      }
      if(!motion && display_on) //if motion is gone and display is still on, display needs to be switched off
      {
        lcd.noDisplay();
        lcd.setBacklight(0);
        display_on = false;
        custom_c++; //switch custom char for next time display on
        if(custom_c==16){custom_c=0;}
        //printf("display: off\n");
      }
      delay(100);
    }
}

//_________________________________________________________________________check plants task
void check_plants(void * pvParameters)
{
    printf("check_plants() running on core: %i\n",xPortGetCoreID());
    for( ;; )
    {
      measure();
      if((dry_flag[0]||dry_flag[1]) && !pump_alarm)
      {
        watering_routine();
      }
      vTaskDelay(SECOND/portTICK_PERIOD_MS);  //for testing
      //vTaskDelay((SECONDS_PER_MINUTE*MINUTES_PER_HOUR*1000)/portTICK_PERIOD_MS) //every hour
    }
}

//_________________________________________________________________________loop
void loop(){vTaskDelay(portMAX_DELAY);}

//_________________________________________________________________________connect to a WiFi
void connect_WiFi(char ssid[],char passwd[])
{
  Serial.println("Connecting to WiFi...");
  lcd.home();
  lcd.print("waiting for WiFi");
  WiFi.begin(ssid, passwd);

  lcd.setCursor(0,1);
  while(WiFi.status()!=WL_CONNECTED){
    wait_animation();
  }
  if(WiFi.status()==WL_CONNECTED){
    Serial.print("Connected to: "); Serial.println(WiFi.SSID());
    Serial.print("IP: "); Serial.println(WiFi.localIP());
    Serial.print("Signal: "); Serial.println(WiFi.RSSI());
    lcd.clear();
    lcd.home();
    lcd.print("connected");
  }
  else{
    printf("WiFi status: %i\n", WiFi.status());
  }
}
//_________________________________________________________________________disconnect WiFi
void disconnect_WiFi()
{
  long timestamp = millis();
  WiFi.disconnect();
  while(millis()-timestamp<10000){
    if(WiFi.status() == WL_DISCONNECTED){
      break;
    }
  }
  Serial.print("WiFi status: "); Serial.println(WiFi.status());
}
//_________________________________________________________________________send UDP packet
void send_UDP_packet(uint8_t buffer[],int udpPort, IPAddress address, int ntpPort)
{
  lcd.setCursor(0,1);
  lcd.print("fetching time...");
  long timestamp;
  Serial.println("opening UDP port...");
  while(true){
    if(udp.begin(udpPort)){
      break;
    } 
    else{
      timestamp = millis();
      while(millis()-timestamp<2000){
      }
      Serial.println("retry...");
    }
  }

  if(!udp.beginPacket(address, ntpPort)){
    Serial.println("sth. wrong!");
  }
  
  buffer[0] = 0b00100011;   //leap-indicator: no alarm, version: 4, mode: client
  buffer[1] = 0;            //Stratum: unspecified

  Serial.println("sending packet...");

  udp.write(buffer,48);
  while(true){
    if(udp.endPacket()){
      break;
    } 
    else{
      timestamp = millis();
      while(millis()-timestamp<2000){
      }
      Serial.println("sending...");
    }
  }
  return;
}
//_________________________________________________________________________recieve UDP packet
void recieve_UDP_packet(byte buffer[])
{
  memset(buffer,0,48);
  long timestamp;
  int size = 0;
  Serial.println("waiting for packet...");
  while(true){
    size=udp.parsePacket();
    if(size){
      Serial.printf("recieved %d bytes from ",size); Serial.println(udp.remoteIP().toString()+":"+String(udp.remotePort()));
      break;
    } 
    else{
      timestamp = millis();
      while(millis()-timestamp<2000){
      }
      Serial.println("waiting...");
    }
  }

  Serial.println("reading...");
  udp.read(buffer,size);

  Serial.print("raw data: ");
  for(int i=0; i<48; i++){
    Serial.printf("%x ",buffer[i]);
  }
  Serial.println("\nUDP stop");
  udp.stop();
  return;
}
//_________________________________________________________________________extract & calculate time from packet
uint32_t extract_time(byte buffer[])
{
  uint32_t total_seconds = 0;
  for(int i=0; i<4; i++){
    total_seconds |= buffer[40+i]<<(24-8*i);
  }
  Serial.printf("%u seconds since 00:00:00 UTC 1/1/1900\n",total_seconds);
  uint32_t seconds_this_day = total_seconds%SECONDS_PER_DAY;
  uint8_t seconds = seconds_this_day%60;  //60 seconds a minute
  uint8_t minutes = ((seconds_this_day-seconds)%(3600))/60;
  uint8_t hours = (seconds_this_day-seconds-minutes*60)/3600 + 1; // +1 for CET as recieved time is UTC
  Serial.printf("time: %d:%d:%d CET\n",hours,minutes,seconds);
  current_time.second = seconds;
  current_time.minute = minutes;
  current_time.hour = hours;
  return total_seconds;
}
//_________________________________________________________________________calculate date from packet
void calc_date(uint32_t total_seconds)
{
  uint32_t seconds_since_last_leap_period = ((total_seconds+SECONDS_PER_DAY)%LEAP_INTERVAL); //-SECONDS_PER_DAY; //seconds since last leap year
  //printf("sSLLP: %i\n",secondsSinceLastLeapPeriod);
  if(seconds_since_last_leap_period <= (SECONDS_PER_YEAR+SECONDS_PER_DAY)){ //leap period starts with leap year, so check if passed time is less than 366 days
    current_time.leap_indicator = true;
    current_time.days_in_month[feb] = 29;
  }
  else
  {
    current_time.leap_indicator = false;
  }
  //printf("indicator: %i\n",current_time.leapIndicator);
  uint8_t finished_leap_periods = ((total_seconds-seconds_since_last_leap_period+SECONDS_PER_DAY)/(LEAP_INTERVAL)); //finished leap periods, missing leap day in 1900 considered in calculation
  uint8_t passed_leap_days = finished_leap_periods;
  if(seconds_since_last_leap_period >= (current_time.days_in_month[jan]+current_time.days_in_month[feb])){  //add 1 day, if today is past 29.feb
    passed_leap_days += 1;
  }
  //printf("passed leap days: %i\n",passed_leap_days);
  uint32_t seconds_this_year = 0; //seconds since beginning of the year  

  if(current_time.leap_indicator)
  {
    seconds_this_year = seconds_since_last_leap_period;
  }
  else
  {
    seconds_this_year = (seconds_since_last_leap_period-SECONDS_PER_DAY)%(SECONDS_PER_YEAR);
  }
  //printf("sThisY: %i\n",seconds_this_year);

  uint32_t seconds_this_day = total_seconds%(SECONDS_PER_DAY); //seconds since beginning of the day
  uint16_t passed_days_this_year = (seconds_this_year-seconds_this_day)/(SECONDS_PER_DAY);  //passed days since beginning of the year
  uint16_t passed_days = (total_seconds-seconds_this_day)/(SECONDS_PER_DAY);  //days since beginning 1900
  uint8_t passed_years = 4*finished_leap_periods + (seconds_since_last_leap_period-seconds_this_year-(current_time.leap_indicator?0:(SECONDS_PER_DAY)))/(SECONDS_PER_YEAR);  //years since 1900
  uint16_t year = 1900+passed_years;
  //printf("sThisDay: %i\npassed daysThisY: %i\npassed days: %i\npassed years: %i\n",seconds_this_day,seconds_this_year,passed_days,passed_years);

  uint8_t month = jan;
  for(int16_t i=0, d=passed_days_this_year; d>=0; d-=current_time.days_in_month[i], i++){
    month ++;
  }

  uint16_t day = passed_days_this_year+1;  //add 1 day, as passed_days_this_year represents only gone days, so current day not included
  for(uint8_t i=1; i<month; i++){
    day -= current_time.days_in_month[i-1];
  }

  uint8_t dayLabel =  passed_days%7; // 1/1/1900 was a Monday
  current_time.yy = year;
  current_time.mm = month;
  current_time.dd = day;
  current_time.day_label = dayLabel;
  current_time.passed_days = passed_days;
  Serial.printf("date: %d/%d/%d\n", year,month,day);

  calc_season_shift(passed_days, passed_days_this_year);
  return;
}
//_________________________________________________________________________calculate the dates of summer/winter shift from timedate instance
//summer shift: +1 hour on 3rd sunday of march, winter shift: -1 hour on 3rd sunday of october, (03:00:00)
void calc_season_shift(uint16_t passed_days, uint16_t passed_days_this_year)
{
  uint8_t march_1th_label = (passed_days-passed_days_this_year+current_time.days_in_month[jan]+current_time.days_in_month[feb])%7; //determine day of march 1th
  uint8_t day_of_march = 1;
  while(march_1th_label < sun){ //count up to the first sunday in march
    march_1th_label++;
    day_of_march++;
  }
  while(day_of_march+sun+1 <= current_time.days_in_month[mar]){ //count to last sunday in month in steps of 7.
    day_of_march += sun+1;
  }

  //repeat for october
  uint16_t temp_days = 0;
  for(uint8_t month=jan; month!=oct; month++){
    temp_days += current_time.days_in_month[month];
  }
  uint8_t october_1th_label = (passed_days-passed_days_this_year+temp_days)%7;
  uint8_t day_of_october = 1;
  while(october_1th_label < sun){
    october_1th_label++;
    day_of_october++;
  }
  while(day_of_october+sun+1 <= current_time.days_in_month[oct]){
    day_of_october += sun+1;
  }
  
  current_time.season_shift[0] = day_of_march;
  current_time.season_shift[1] = day_of_october;
  Serial.printf("summer/winter shift: %dth of March, %dth of October\n",day_of_march,day_of_october);
  return;
}
//_________________________________________________________________________counts up seconds
void IRAM_ATTR count_ISR(void)
{
  current_time.second++;
  t_update = true;  //indicate time has changed
  return;
}
//_________________________________________________________________________rollover sec/min/hour/day/month/year and daylabel
void rollover(void)
{ 
  t_update = true;
  current_time.second = 0;
  current_time.minute++;
  //hour rollover
  if(current_time.minute == MINUTES_PER_HOUR){
    current_time.minute = 0;
    current_time.hour++;
    //following section is responsible for season shifting. needs to be checked at 03:00:00
    if((current_time.mm == mar && current_time.dd == current_time.season_shift[0]) || (current_time.mm == oct && current_time.dd == current_time.season_shift[1])){
      if(current_time.hour == 3 && !current_time.shifted){
          switch(current_time.mm){
            case mar: current_time.hour++; current_time.shifted = true; break;
            case oct: current_time.hour--; current_time.shifted = true; break;
          }
        }
      if(current_time.shifted && current_time.hour == 4){  //control variable reset at 04:00:00
        current_time.shifted = false;
      }
    }
    //day rollover
    if(current_time.hour == HOURS_PER_DAY){
      current_time.hour = 0;
      current_time.dd++;
      current_time.day_label++;
      d_update = true;
      //daylabel rollover
      if(current_time.day_label > sun){
        current_time.day_label = mon;
      }
      //month rollover
      if(current_time.dd > current_time.days_in_month[current_time.mm-1]){
        current_time.dd = 1;
        current_time.mm++;
        //year rollover
        if(current_time.mm > sizeof(Month)){
          current_time.mm = jan+1;
          current_time.yy++;
          if(current_time.yy%4){
            current_time.leap_indicator = true;
            current_time.days_in_month[feb] = 29;
          }
          else{
            current_time.leap_indicator = false;
            current_time.days_in_month[feb] = 28;
          }
          if(current_time.yy == CRITICAL_YEAR){
            //warning on display here? <-----------------------------
          }
        }
      }
    }
  }
  return;
}
//_________________________________________________________________________update display output
void update_output(void)
{
  lcd.setBacklight(255);
  lcd.display();

  int *custom = get_custom();
  lcd.createChar(0,custom);
  lcd.setCursor(0,1);
  lcd.write(byte(0));

  if(pump_alarm)
  {
    lcd.setCursor(2,1);
    lcd.print("ALARM");
  }

  String time = (current_time.hour<10?"0":"")+String(current_time.hour)+":"+(current_time.minute<10?"0":"")
    +String(current_time.minute)+":"+(current_time.second<10?"0":"")+String(current_time.second);
  Serial.print(time+"\r");
  lcd.setCursor(8,1);
  lcd.print(time);

  if(d_update)
  {
    String date = day_verbose[current_time.day_label]+"   "+(current_time.dd<10?"0":"")+String(current_time.dd)+"."
      +(current_time.mm<10?"0":"")+String(current_time.mm)+"."+(current_time.yy<10?"0":"")+String(current_time.yy);
    lcd.setCursor(0,0);
    lcd.print(date);
    d_update = false;
  }
  t_update = false;
}
//_________________________________________________________________________timer/interrupt, analog in, digital out configuration
void hardware_config(void)
{  
  //analog digital converter configuration
  adc1_config_width(ADC_WIDTH_12Bit);
  adc1_config_channel_atten(MOISTURE_0,ADC_ATTEN_11db);
  adc1_config_channel_atten(MOISTURE_1,ADC_ATTEN_11db);
  //timer configuration: microseconds,t1 autoreload with interrupt each second, t2 just counts up
  timer1 = timerBegin(0,80,true);
  timer2 = timerBegin(0,80,true);
  timerAttachInterrupt(timer1,count_ISR,false);
  timerAlarmWrite(timer1,T_INTR_INTERVAL,true);
  timerAlarmEnable(timer1);
  //PIR sensor input pullup, trigger on rising and falling edge for display control
  pinMode(PIR_PIN,INPUT);
  while(digitalRead(PIR_PIN)){delay(100);}
  attachInterrupt(PIR_PIN,pir_ISR,CHANGE);
  //digital out for P,S1,S2 MOSFET control
  pinMode(SOLENOID_0,OUTPUT);
  pinMode(SOLENOID_1,OUTPUT);
  pinMode(PUMP,OUTPUT);
}
//_________________________________________________________________________measure moisture
void measure(void)
{ 
  uint16_t m0 = 0;
  uint16_t m1 = 0;
  
  for(int i=0; i<10; i++)
  {
    m0 += adc1_get_raw(MOISTURE_0);
    m1 += adc1_get_raw(MOISTURE_1);
    vTaskDelay(MEAS_SAMPLE_PAUSE/portTICK_PERIOD_MS);
  }
  m0 /=10;
  m1 /=10;


  if(m0>DRY)
  {
    dry_flag[0] = true;
  }
  else
  {
    dry_flag[0] = false;
  }
  if(m1>DRY)
  {
    dry_flag[1] = true;
  }
  else
  {
    dry_flag[1] = false;
  }
}
//_________________________________________________________________________switch actuator ON/OFF
void switch_actuator(gpio_num_t pin, bool state)
{
  String actuator = "";
  switch(pin)
  {
    case SOLENOID_0: actuator = "S1"; break;
    case SOLENOID_1: actuator = "S2"; break;
    case PUMP: actuator = "pump"; break;
    default: actuator = "err"; break;
  }
  if(state)
  {
    digitalWrite(pin,HIGH);
    Serial.print(actuator); Serial.println(" on");
  }
  else
  {
    digitalWrite(pin,LOW);
    Serial.print(actuator); Serial.println(" off");
  }
}
//_________________________________________________________________________PIR ISR
void IRAM_ATTR pir_ISR(void)
{
  motion = !motion;
  return;
}
//_________________________________________________________________________watering routine
void watering_routine(void)
{
  while(!routine_done[0] || !routine_done[1])
  {
    if(dry_flag[0] && cycle_counter[0]<CYCLES)
    {
      switch_actuator(SOLENOID_0,ON);//on
      switch_actuator(PUMP,ON);//on      
      vTaskDelay(WATERING_INTERVAL/portTICK_PERIOD_MS);
      switch_actuator(SOLENOID_0,OFF);//off
      switch_actuator(PUMP,OFF);//off
      cycle_counter[0]++;
    }
    else
    {
      routine_done[0] = true;
    }
    vTaskDelay(SECOND);
    if(dry_flag[1] && cycle_counter[1]<CYCLES)
    {
      switch_actuator(SOLENOID_1,ON);//on
      switch_actuator(PUMP,ON);//on      
      vTaskDelay(WATERING_INTERVAL/portTICK_PERIOD_MS);
      switch_actuator(SOLENOID_1,OFF);//off
      switch_actuator(PUMP,OFF);//off
      cycle_counter[1]++;
    }
    else
    {
      routine_done[1] = true;
    }
    measure();
    vTaskDelay(10*SECOND);
    measure();
  }

  if(cycle_counter[0] >= CYCLES || cycle_counter[1] >= CYCLES)
  {
    pump_alarm = true;
  }
  else
  {
    routine_done[0] = false;
    routine_done[1] = false;
    cycle_counter[0] = 0;
    cycle_counter[1] = 0;
  }
  printf("al: %i\ncycle(0): %i\ncycle(1): %i\n",pump_alarm,cycle_counter[0],cycle_counter[1]);
}

void wait_animation()
{
  static long timestamp = millis();
  static int i = 0;
  static bool direction = true;
  if(millis()-timestamp>100){
      lcd.print(" ");
      lcd.setCursor(i-1,1);
      lcd.print("#");
      timestamp = millis();
      if(i==15){direction=false;}
      else if(i==0){direction = true;}
      if(direction){i++;}
      else i--;
  }
}

void custom_char_animtion()
{
  lcd.createChar(0,twoleg);
  lcd.createChar(1,hourglass);
  lcd.createChar(2,lock);
  lcd.createChar(3,unicorn);
  lcd.createChar(4,hippo);
  lcd.createChar(5,skull);
  lcd.createChar(6,spider);
  lcd.createChar(7,umbrella);

  lcd.setCursor(0,0);
  lcd.write(byte(0));
  delay(500);
  lcd.setCursor(1,0);
  lcd.write(byte(1));
  delay(500);
  lcd.setCursor(2,0);
  lcd.write(byte(2));
  delay(500);
  lcd.setCursor(3,0);
  lcd.write(byte(3));
  delay(500);
  lcd.setCursor(4,0);
  lcd.write(byte(4));
  delay(500);
  lcd.setCursor(5,0);
  lcd.write(byte(5));
  delay(500);
  lcd.setCursor(6,0);
  lcd.write(byte(6));
  delay(500);
  lcd.setCursor(7,0);
  lcd.write(byte(7));
  delay(500);
  int i = 8;
  while(i)
  {
    lcd.scrollDisplayRight();
    delay(500);
    i--;
  }
  i=8;
  while(i)
  {
    lcd.scrollDisplayLeft();
    delay(200);
    i--;
  }
  lcd.home();
}

int * get_custom()
{
  switch (custom_c)
  {
    case 0: return twoleg;
    case 1: return hourglass;
    case 2: return lock;
    case 3: return unicorn;
    case 4: return hippo;
    case 5: return skull;
    case 6: return spider;
    case 7: return umbrella;
    case 8: return deer;
    case 9: return bandit;
    case 10: return happy;
    case 11: return mouse;
    case 12: return trophy;
    case 13: return human;
    case 14: return giraffe;
    case 15: return eye;
    default: return spider;
  }
}